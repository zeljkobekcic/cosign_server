package main

import (
	"encoding/json"
	"net/http"
	"os/exec"
)

type VerificationRequst struct {
	Image string `json:"image"`
}

type VerificationResponse struct {
	Verified bool `json:"verified"`
}

func runCosignVerify(imgName string) bool {
	cmd := exec.Command("cosign", "verify", "--key", "cosign.pub", imgName)
	cmd.Run()
	return cmd.ProcessState.ExitCode() == 0
}

func handleCosignVerifyRequest(w http.ResponseWriter, r *http.Request) {
	var verificationRequest VerificationRequst
	json.NewDecoder(r.Body).Decode(&verificationRequest)

	var verificationResponse = VerificationResponse{
		Verified: runCosignVerify(verificationRequest.Image),
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(verificationResponse)

}

func main() {
	http.HandleFunc("/", handleCosignVerifyRequest)
	http.ListenAndServe(":8080", nil)
}
