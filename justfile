DOCKER_IMAGE := "registry.gitlab.com/zeljkobekcic/cosign_server"

dockerPull:
    docker pull {{ DOCKER_IMAGE }}

dockerRun:
    docker run -p 8080:8080 {{ DOCKER_IMAGE }}

dockerBuild:
    docker build -t {{ DOCKER_IMAGE }} .

dockerPush:
    docker push {{ DOCKER_IMAGE }}

sendRequest:
    curl -X POST -H "Content-type: application/json" -d '{ "image": "iits/git-jq:1.2.0" }' 'localhost:8080'