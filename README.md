# Cosign Server (WIP 🚧👷‍)

This server calls cosign on a given image and return true or false given the provided public key.

## Usage

```shell
just dockerPull
just dockerRun

# in another shell
just sendRequest
# this should return
# {"verified": true}
```