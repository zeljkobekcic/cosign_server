FROM golang:1.18-alpine as builder
WORKDIR /go/src/github.com/iits-consulting/cosign_server
COPY . .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -o app .

FROM alpine:3.16
RUN wget "https://github.com/sigstore/cosign/releases/download/v1.6.0/cosign-linux-amd64" && \
    mv cosign-linux-amd64 /usr/local/bin/cosign && \
    chmod +x /usr/local/bin/cosign

# TODO copy dockerconfigjson into the container
# /root/.docker/config.json


WORKDIR /app
COPY --from=builder /go/src/github.com/iits-consulting/cosign_server/app /app
COPY cosign.pub /app
CMD [ "/app/app" ]

